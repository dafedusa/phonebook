package services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.LinkedList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import Exceptions.ContactExistsException;
import beans.Contact;

public class queries 
{
	private static DataSource dataSource;
	private static Connection connection;
	private static Statement statement;
	public static final int pageSize = 10;
	
	public static void createTable()
	{
		try 
		{
			Context initContext  = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			dataSource = (DataSource)envContext.lookup("jdbc/contacts");
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			String query = "CREATE TABLE contacts " +
			           "(FirstName VARCHAR(50), " +
			           " LastName VARCHAR(50), " + 
			           " Phone VARCHAR(10), " + 
			           " PRIMARY KEY (Phone))"; 
			statement.executeUpdate(query);
		} 
		catch (SQLException e) 
		{
			if(!e.getMessage().equals("Table 'contacts' already exists"))
				e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			try { if(null!=statement)statement.close();} catch (SQLException e) 
			{e.printStackTrace();}
			try { if(null!=connection)connection.close();} catch (SQLException e) 
			{e.printStackTrace();}
		}
	}
	
	public static Iterator<Contact> search(String searchParam, int page)
	{
		createTable();
		try 
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			String query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'contacts'";
			ResultSet rs = statement.executeQuery(query);
			query = "SELECT * FROM contacts WHERE ";
			while(rs.next())
			{
				query=query.concat(rs.getString("COLUMN_NAME")+" LIKE '%"+searchParam+"%' OR ");
			}
			query = query.substring(0, query.length()-4);
			query = query.concat(" LIMIT "+(pageSize*(page-1))+","+pageSize);
			rs = statement.executeQuery(query);
			return ResultSetToList(rs);
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			try { if(null!=statement)statement.close();} catch (SQLException e) 
			{e.printStackTrace();}
			try { if(null!=connection)connection.close();} catch (SQLException e) 
			{e.printStackTrace();}
		}
		return null;
	}
	
	public static void add(Contact c) throws ContactExistsException
	{
		createTable();
		Iterator<Contact> iter = search(c.getPhone(),1);
		if(!iter.hasNext())
		{
			try
			{
				connection = dataSource.getConnection();
				statement = connection.createStatement();
				String query = "INSERT INTO contacts " +
				           "(FirstName, LastName, Phone) " + 
				           "VALUES ('"
				           +c.getFirstName()+"', '"
				           +c.getLastName()+"', '"
				           +c.getPhone()+"')";
				statement.executeUpdate(query);
			}
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			finally
			{
				try { if(null!=statement)statement.close();} catch (SQLException e) 
				{e.printStackTrace();}
				try { if(null!=connection)connection.close();} catch (SQLException e) 
				{e.printStackTrace();}
			}
			
		}
		else
		{
			throw new ContactExistsException("The phone number "+c.getPhone()+" has already been added.");
		}
	}
	
	public static int getSearchSize(String searchParam)
	{
		createTable();
		try 
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			String query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'contacts'";
			ResultSet rs = statement.executeQuery(query);
			query = "SELECT * FROM contacts WHERE ";
			while(rs.next())
			{
				query=query.concat(rs.getString("COLUMN_NAME")+" LIKE '%"+searchParam+"%' OR ");
			}
			query = query.substring(0, query.length()-4);
			rs = statement.executeQuery(query);
			rs.last();
			int nrow = rs.getRow();
			return nrow;
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			try { if(null!=statement)statement.close();} catch (SQLException e) 
			{e.printStackTrace();}
			try { if(null!=connection)connection.close();} catch (SQLException e) 
			{e.printStackTrace();}
		}
		return 0;
	}
	
	private static Iterator<Contact> ResultSetToList(ResultSet rs)
	{
		LinkedList<Contact> ll = new LinkedList<Contact>();
		try 
		{
			while(rs.next())
			{
				Contact c = new Contact(rs.getString("FirstName"), rs.getString("LastName"), rs.getString("Phone"));
				ll.add(c);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ll.iterator();		
	}
}
