package servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Exceptions.ContactExistsException;
import Exceptions.ContactFormatException;
import beans.Contact;
import services.queries;


@WebServlet("/add")
public class addServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
   {
	  String firstName = request.getParameter("First Name");
	  String lastName = request.getParameter("Last Name");
	  String phone = request.getParameter("Phone");
	  phone = phone.replace('-', ' ');
	  phone = phone.trim();
	  String message = null;
	  Boolean error = null;
	  try 
	  {
		if(phone.matches("[0-9]+") && phone.length() == 10)
		{
			if(firstName!=null && !firstName.equals(""))
			{
				Contact c = new Contact(firstName, lastName, phone);
				queries.add(c);
				error = new Boolean(false);
				message = "Contact added successfully";
			}
			else
			{
				throw new ContactFormatException("The new contact must have a first name");
			}
		}
		else
		{
			throw new ContactFormatException("The phone number must be a 10 digit sequence");
		}
		
	  } 
	  catch (ContactExistsException | ContactFormatException e) 
	  {
		error = new Boolean(true);
		message = e.getMessage();
	  }
	  
	  request.setAttribute("addError", error);
	  request.setAttribute("addMessage", message);
	  RequestDispatcher dispatcher =request.getRequestDispatcher("index");
	  dispatcher.forward(request, response);
   }

}
