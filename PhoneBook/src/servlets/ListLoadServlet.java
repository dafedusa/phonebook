package servlets;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Contact;
import services.queries;

@SuppressWarnings("serial")
@WebServlet("/index")
public class ListLoadServlet extends HttpServlet 
{
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		Iterator<Contact> iter = queries.search("",1);
		RequestDispatcher dispatcher =request.getRequestDispatcher("contacts.jsp");
		request.setAttribute("contacts", iter);
		request.setAttribute("page", new Integer(1));
		request.setAttribute("searchParam", "");
		dispatcher.forward(request, response);
	}
	
}
