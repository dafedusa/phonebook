package servlets;

import java.io.IOException;
import java.util.Iterator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Contact;
import services.queries;

/**
 * Servlet implementation class SearchServlet
 */
@SuppressWarnings("serial")
@WebServlet("/search")
public class SearchServlet extends HttpServlet 
{	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		int add = 0;
		int page = Integer.parseInt(request.getParameter("page"));
		if(request.getParameter("dir")!=null)
		{
			if(request.getParameter("dir").equals("prev"))
			{
				add= -1;
			}
			else if(request.getParameter("dir").equals("next"))
			{
				add= 1;
			}
		}
		page+=add;
		int nrows = queries.getSearchSize(request.getParameter("searchParam"));
		if(page==0)page=1;
		else if(page>nrows/queries.pageSize+(nrows%queries.pageSize==0?0:1))page--;
		
		Iterator<Contact> iter = queries.search(request.getParameter("searchParam"),page);
		RequestDispatcher dispatcher =request.getRequestDispatcher("contacts.jsp");
		request.setAttribute("contacts", iter);
		request.setAttribute("page",page);
		request.setAttribute("searchParam", request.getParameter("searchParam"));
		dispatcher.forward(request, response);
	}

}
