package Exceptions;

@SuppressWarnings("serial")
public class ContactFormatException extends Exception 
{
	public ContactFormatException(String message)
	{
		super(message);
	}
}
