package Exceptions;

@SuppressWarnings("serial")
public class ContactExistsException extends Exception 
{
	public ContactExistsException(String message)
	{
		super(message);
	}
}
