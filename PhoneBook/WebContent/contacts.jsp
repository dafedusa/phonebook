<%@page import="java.util.Iterator"%>
<%@page import="beans.Contact"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
	<title>PhoneBook</title>
	<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/styles.css">
</head>
<body>
	<div class="container">
		<div class="pure-g">
			<div class="pure-u-1">
				<div class="header">
					<img class="logo" src="img/phonebook.png"/>
					<p>v 1.0</p>
				</div>
				
			</div>
		</div>
		<div class="pure-g">
		    <div class="pure-u-sm-1 pure-u-1-3">
		    	<div class="box">
		    		<h2><i class="fa fa-user-plus"></i>New contact</h2>
		    		<form class="pure-form" method="get" action="add">
					    <fieldset class="pure-group">
					        <input type="text" class="pure-input-1-2" placeholder="First Name" name="First Name">
					        <input type="text" class="pure-input-1-2" placeholder="Last Name" name="Last Name">
					        <input type="text" class="pure-input-1-2" placeholder="Phone" name="Phone">
					    </fieldset>
					    <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
					    <i class="fa fa-user-plus"></i>Add</button>
					</form>
					<%
						Boolean addError = (Boolean)request.getAttribute("addError");
						String addMessage = (String)request.getAttribute("addMessage");
						if(addError!=null && addError.booleanValue() && addMessage!= null & !addMessage.equals(""))
						{%>
							<h4><%=addMessage.toString()%></h4>
						<%}
						else
						{
							if(addMessage!= null && !addMessage.equals(""))
							{%>
							<h3><%=addMessage.toString()%></h3>
						  <%}%>
					  <%}%>
				</div>
			</div>
		    <div class="pure-u-sm-1 pure-u-1-3">
				<div class="box">
		    		<h2><i class="fa fa-search"></i>Search contact</h2>
		    		<form class="pure-form" method="get" action="search">
		    			<fieldset class="pure-group">
					    	<input type="text" class="pure-input-1-2" name="searchParam">
					    	<input type="hidden" class="pure-input-1-2" name="page" value=<%=("1").toString()%>>
					     </fieldset>
					    <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">					    
					    <i class="fa fa-search"></i>Search</button>
					</form>
				</div>
			</div>
			<div class="pure-u-sm-1 pure-u-1-3">
				<div class="boxContacts">
		    		<h2><i class="fa fa-users"></i> Contacts</h2>
	    			<table class="pure-table">
					    <thead>
					        <tr>
					            <th>First Name</th>
					            <th>Last Name</th>
					            <th>Phone</th>
					        </tr>
					    </thead>
					
					    <tbody>
					    <%
					    Iterator<Contact> iter = (Iterator<Contact>)request.getAttribute("contacts");
					    if(iter!=null)
					    {
					    	while(iter.hasNext())
					    	{
					    		Contact c = iter.next(); 
					    %>
					        <tr>
					            <td><%=c.getFirstName()%></td>
					            <td><%=c.getLastName()%></td>
					            <%
					            c.setPhone(c.getPhone().substring(0,3)+"-"+c.getPhone().substring(3,10));
					            %>					         
					            <td><%=c.getPhone()%></td>
					        </tr>
					    <%}}
					    %>
					    </tbody>
					</table>
					<br>
					<% 
					Integer numpage = (Integer)request.getAttribute("page"); 
					String searchParam = (String)request.getAttribute("searchParam");
					if(numpage!=null && searchParam !=null)
					{%>
						<table align="center">
							<tr>
								<th>
									<form method="get" action="search">
										<button type="submit" class="pure-button pure-input-1-2 pure-button-primary">&lt&lt</button>
										<input type="hidden" class="pure-input-1-2" name="searchParam" value=<%=searchParam.toString()%>>
										<input type="hidden" class="pure-input-1-2" name="page" value=<%=(numpage.intValue()+"").toString()%>>
										<input type="hidden" class="pure-input-1-2" name="dir" value=<%=("prev").toString()%>>
									</form>									
								</th>
								<th>
									<form method="get" action="search">
										<button type="submit" class="pure-button pure-input-1-2 pure-button-primary">&gt&gt</button>
										<input type="hidden" class="pure-input-1-2" name="searchParam" value=<%=searchParam.toString()%>>
										<input type="hidden" class="pure-input-1-2" name="page" value=<%=(numpage.intValue()+"").toString()%>>
										<input type="hidden" class="pure-input-1-2" name="dir" value=<%=("next").toString()%>>
									</form>
								</th>
							</tr>
						</table>
				  <%}
					%>				    
				</div>
			</div>
		</div>
	</div>
</body>
</html>